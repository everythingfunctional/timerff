module timerff_impl
    use quaff, only: time_t, operator(.unit.), MILLISECONDS, SECONDS

    implicit none
    private

    public :: &
            timer_t, &
            start_timer, &
            stop_timer, &
            measure_run_time, &
            time_between_dates, &
            day_of_year, &
            is_leap_year, &
            count_kind

    integer, parameter :: count_kind = selected_int_kind(10)

    type :: timer_t
        !! Derived type to encapsulate timing implementation
        private
        integer(count_kind) :: system_start, system_rate, system_max, system_finish
        integer(count_kind) :: date_time_start(8), date_time_finish(8)
        type(time_t), public :: system_time, wall_time
    end type
contains
    subroutine start_timer(timer)
        !! Initialize a timer
        type(timer_t), intent(out) :: timer

        call system_clock(count=timer%system_start, count_rate=timer%system_rate, count_max=timer%system_max)
        call date_and_time(values=timer%date_time_start)
    end subroutine

    subroutine stop_timer(timer)
        !! Calculate timings since most recent call to [[start_timer]] with this timer
        type(timer_t), intent(inout) :: timer

        call system_clock(count=timer%system_finish)
        call date_and_time(values=timer%date_time_finish)

        call calculate_system_time(timer)
        call calculate_wall_time(timer)
    end subroutine

    subroutine measure_run_time(proc, samples, average_system_time, average_wall_time)
        !! Measure the average run time of a procedure by running it [[samples]] times
        interface
            subroutine proc
            end subroutine
        end interface
        integer, intent(in) :: samples
        type(time_t), intent(out) :: average_system_time, average_wall_time

        integer :: i
        type(timer_t) :: timer

        call start_timer(timer)
        do i = 1, samples
            call proc
        end do
        call stop_timer(timer)
        average_system_time = timer%system_time / samples
        average_wall_time = timer%wall_time / samples
    end subroutine

    subroutine calculate_system_time(timer)
        type(timer_t), intent(inout) :: timer

        integer(count_kind) :: elapsed

        elapsed = timer%system_finish - timer%system_start
        if (elapsed <= 0) elapsed = elapsed + timer%system_max
        timer%system_time = ( &
                real(elapsed, kind=kind(timer%system_time.in.SECONDS)) &
                / real(timer%system_rate, kind=kind(timer%system_time.in.SECONDS))).unit.SECONDS
    end subroutine

    subroutine calculate_wall_time(timer)
        type(timer_t), intent(inout) :: timer

        timer%wall_time = time_between_dates(timer%date_time_start, timer%date_time_finish)
    end subroutine

    pure function time_between_dates(begin, finish) result(time)
        integer(count_kind), intent(in) :: begin(8), finish(8)
        type(time_t) :: time

        integer(count_kind) :: before(8), after(8)
        logical :: negative
        integer(count_kind) :: day_of_year_before, day_of_year_after
        integer(count_kind) :: years, days, hours, minutes, seconds
        integer(count_kind) :: year

        if (begin(1) > finish(1)) then
            before = finish
            after = begin
            negative = .true.
        else if (finish(1) > begin(1)) then
            before = begin
            after = finish
            negative = .false.
        else if (begin(2) > finish(2)) then ! Same year
            before = finish
            after = begin
            negative = .true.
        else if (finish(2) > begin(2)) then
            before = begin
            after = finish
            negative = .false.
        else if (begin(3) > finish(3)) then ! Same month
            before = finish
            after = begin
            negative = .true.
        else if (finish(3) > begin(3)) then
            before = begin
            after = finish
            negative = .false.
        else if (begin(5) > finish(5)) then ! Same day
            before = finish
            after = begin
            negative = .true.
        else if (finish(5) > begin(5)) then
            before = begin
            after = finish
            negative = .false.
        else if (begin(6) > finish(6)) then ! Same hour
            before = finish
            after = begin
            negative = .true.
        else if (finish(6) > begin(6)) then
            before = begin
            after = finish
            negative = .false.
        else if (begin(7) > finish(7)) then ! Same minute
            before = finish
            after = begin
            negative = .true.
        else if (finish(7) > begin(7)) then
            before = begin
            after = finish
            negative = .false.
        else if (begin(8) > finish(8)) then ! Same second
            before = finish
            after = begin
            negative = .true.
        else if (finish(8) > begin(8)) then
            before = begin
            after = finish
            negative = .false.
        else ! same time
            time = real(0, kind=kind(time%seconds)).unit.MILLISECONDS
            return
        end if

        day_of_year_before = day_of_year(before(1), before(2), before(3))
        day_of_year_after = day_of_year(after(1), after(2), after(3))

        years = after(1) - before(1)
        days = &
                years*365 + day_of_year_after - day_of_year_before + &
                count(is_leap_year([(year, year = before(1), after(1)-1)])) ! add days for leap years
        hours = days*24 + after(5) - before(5)
        minutes = hours*60 + after(6) - before(6)
        seconds = minutes*60 + after(7) - before(7)
        time = real(seconds*1000 + after(8) - before(8), kind=kind(time%seconds)).unit.MILLISECONDS
        if (negative) time = -time
    end function

    pure function day_of_year(year, month, day)
        integer(count_kind), intent(in) :: year, month, day
        integer(count_kind) :: day_of_year

        integer :: days_per_month(12)

        days_per_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        if (is_leap_year(year)) days_per_month(2) = 29
        day_of_year = sum(days_per_month(1:month-1)) + day
    end function

    elemental function is_leap_year(year)
        integer(count_kind), intent(in) :: year
        logical :: is_leap_year

        if (mod(year, 400) == 0) then
            is_leap_year = .true.
        else
            if (mod(year, 100) == 0) then
                is_leap_year = .false.
            else
                is_leap_year = mod(year, 4) == 0
            end if
        end if
    end function
end module

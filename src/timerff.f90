module timerff
    use timerff_impl, only: timer_t, start_timer, stop_timer, measure_run_time
end module
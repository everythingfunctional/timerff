program measure
    use iso_varying_string, only: put_line, operator(//)
    use quaff, only: time_t
    use timerff, only: measure_run_time

    implicit none

    type(time_t) :: system, wall
    real :: matrix(100, 100)

    ! first set up our problem
    call random_number(matrix)

    call measure_run_time(do_stuff, 100, system, wall)

    call put_line("Took: wall: " // wall%to_string() // ", system: " // system%to_string())
contains
    subroutine do_stuff
        ! Just do a bunch of matmuls to simulate a bunch of computation
        integer :: i

        do i = 1, 100
            matrix = matmul(matrix, matrix)
        end do
    end subroutine
end program
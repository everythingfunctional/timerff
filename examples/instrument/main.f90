program instrument
    use iso_varying_string, only: put_line, operator(//)
    use timerff, only: timer_t, start_timer, stop_timer

    implicit none

    type(timer_t) :: timer
    real :: matrix(100, 100)

    ! first set up our problem
    call random_number(matrix)

    call start_timer(timer) ! initialize our timer
    call do_stuff() ! do our work
    call stop_timer(timer) ! stop the timer/calculate time spent

    call put_line("Took: wall: " // timer%wall_time%to_string() // ", system: " // timer%system_time%to_string())
contains
    subroutine do_stuff
        ! Just do a bunch of matmuls to simulate a bunch of computation
        integer :: i

        do i = 1, 100
            matrix = matmul(matrix, matrix)
        end do
    end subroutine
end program
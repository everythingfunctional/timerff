# timerff

A very simple timer library.

A `timer_t` type is provided, with procedures `start_timer` and `stop_timer` to start and stop a timer respectively.
Once stopped, a timer has components of type `time_t` from the [`quaff`](https://gitlab.com/everythingfunctional/quaff) library
containing the elapsed system and wall time since the most recent call to start the timer.

Additionally, a procedure is provided which will run a subroutine (that takes no arguments)
a given number of times and report the average system and wall times taken to execute it.

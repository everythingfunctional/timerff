---
project: timerff
summary: Timer for Fortran
project_website: https://gitlab.com/everythingfunctional/timerff
author: Brad Richardson
email: everythingfunctional@protonmail.com
website: https://everythingfunctional.com
twitter: https://twitter.com/everythingfunct
github: https://github.com/everythingfunctional
src_dir: ../src
preprocessor: gfortran -E
display: public
         protected
         private
sort: permission-alpha
output_dir: ../public
graph: true
...

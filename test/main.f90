! Generated by cart. DO NOT EDIT
program main
    implicit none

    if (.not.run()) stop 1
contains
    function run() result(passed)
        use day_of_year_test, only: &
                day_of_year_day_of_year => &
                    test_day_of_year
        use is_leap_year_test, only: &
                is_leap_year_is_leap_year => &
                    test_is_leap_year
        use time_between_test, only: &
                time_between_time_between => &
                    test_time_between
        use veggies, only: test_item_t, test_that, run_tests



        logical :: passed

        type(test_item_t) :: tests
        type(test_item_t) :: individual_tests(3)

        individual_tests(1) = day_of_year_day_of_year()
        individual_tests(2) = is_leap_year_is_leap_year()
        individual_tests(3) = time_between_time_between()
        tests = test_that(individual_tests)


        passed = run_tests(tests)

    end function
end program

module time_between_test
    use quaff, only: time_t, operator(.unit.), MILLISECONDS, SECONDS, MINUTES, HOURS, DAYS
    use quaff_asserts_m, only: assert_equals
    use timerff_impl, only: time_between_dates, count_kind
    use veggies, only: &
            example_t, input_t, result_t, test_item_t, describe, fail, it

    implicit none
    private
    public :: test_time_between

    type, extends(input_t) :: time_span_input
        integer(count_kind) :: span_start(8), span_end(8)
        type(time_t) :: expected
    end type
contains
    function test_time_between() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
                "calc_time_between_dates", &
                [ it( &
                        "matches some example time spans", &
                        ! Start with single unit cases
                        [ example_t(time_span_input( &
                                span_start = [2000, 1, 2, 0, 3, 4, 5, 6], &
                                span_end = [2000, 1, 2, 0, 3, 4, 5, 7], &
                                expected = 1.d0.unit.MILLISECONDS)) &
                        , example_t(time_span_input( &
                                span_start = [2000, 1, 2, 0, 3, 4, 5, 6], &
                                span_end = [2000, 1, 2, 0, 3, 4, 6, 6], &
                                expected = 1.d0.unit.SECONDS)) &
                        , example_t(time_span_input( &
                                span_start = [2000, 1, 2, 0, 3, 4, 5, 6], &
                                span_end = [2000, 1, 2, 0, 3, 5, 5, 6], &
                                expected = 1.d0.unit.MINUTES)) &
                        , example_t(time_span_input( &
                                span_start = [2000, 1, 2, 0, 3, 4, 5, 6], &
                                span_end = [2000, 1, 2, 0, 4, 4, 5, 6], &
                                expected = 1.d0.unit.HOURS)) &
                        , example_t(time_span_input( &
                                span_start = [2000, 1, 2, 0, 3, 4, 5, 6], &
                                span_end = [2000, 1, 3, 0, 3, 4, 5, 6], &
                                expected = 1.d0.unit.DAYS)) &
                        , example_t(time_span_input( &
                                span_start = [2000, 1, 2, 0, 3, 4, 5, 6], &
                                span_end = [2000, 2, 2, 0, 3, 4, 5, 6], &
                                expected = 31.d0.unit.DAYS)) &
                        , example_t(time_span_input( &
                                span_start = [2000, 1, 2, 0, 3, 4, 5, 6], &
                                span_end = [2001, 1, 2, 0, 3, 4, 5, 6], &
                                expected = 366.d0.unit.DAYS)) & ! 1 leap year
                        , example_t(time_span_input( &
                                span_start = [2001, 1, 2, 0, 3, 4, 5, 6], &
                                span_end = [2002, 1, 2, 0, 3, 4, 5, 6], &
                                expected = 365.d0.unit.DAYS)) & ! 1 regular year
                        ! Now look at some edge cases
                        , example_t(time_span_input( &
                                span_start = [2000, 1, 2, 0, 3, 4, 5, 6], &
                                span_end = [2000, 1, 2, 0, 3, 4, 5, 6], &
                                expected = 0.d0.unit.DAYS)) & ! same time
                        , example_t(time_span_input( &
                                span_start = [2000, 1, 2, 0, 3, 4, 5, 999], &
                                span_end = [2000, 1, 2, 0, 3, 4, 6, 1], &
                                expected = 2.d0.unit.MILLISECONDS)) & ! next second
                        , example_t(time_span_input( &
                                span_start = [2000, 1, 2, 0, 3, 4, 59, 999], &
                                span_end = [2000, 1, 2, 0, 3, 5, 0, 1], &
                                expected = 2.d0.unit.MILLISECONDS)) & ! next minute
                        , example_t(time_span_input( &
                                span_start = [2000, 1, 2, 0, 3, 59, 59, 999], &
                                span_end = [2000, 1, 2, 0, 4, 0, 0, 1], &
                                expected = 2.d0.unit.MILLISECONDS)) & ! next hour
                        , example_t(time_span_input( &
                                span_start = [2000, 1, 2, 0, 23, 59, 59, 999], &
                                span_end = [2000, 1, 3, 0, 0, 0, 0, 1], &
                                expected = 2.d0.unit.MILLISECONDS)) & ! next day
                        , example_t(time_span_input( &
                                span_start = [2000, 1, 31, 0, 23, 59, 59, 999], &
                                span_end = [2000, 2, 1, 0, 0, 0, 0, 1], &
                                expected = 2.d0.unit.MILLISECONDS)) & ! next month
                        , example_t(time_span_input( &
                                span_start = [2000, 12, 31, 0, 23, 59, 59, 999], &
                                span_end = [2001, 1, 1, 0, 0, 0, 0, 1], &
                                expected = 2.d0.unit.MILLISECONDS)) & ! next year
                        , example_t(time_span_input( &
                                span_start = [1999, 1, 2, 0, 3, 4, 5, 6], &
                                span_end = [2005, 1, 2, 0, 3, 4, 5, 6], &
                                expected = (365.d0*6+2).unit.DAYS)) & ! multiple leap years
                        ], &
                        check_time_span) &
                ])
    end function

    pure function check_time_span(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (time_span_input)
            result_ = assert_equals( &
                    input%expected, &
                    time_between_dates(input%span_start, input%span_end), &
                    "start: " // date_to_string(input%span_start) // ", end: " // date_to_string(input%span_end))
        class default
            result_ = fail("Didn't get a time span")
        end select
    end function

    pure function date_to_string(date) result(string)
        integer(count_kind), intent(in) :: date(8)
        character(len=23) :: string

        character(len=*), parameter :: fmt = &
                "(I4.2,'-',I2.2,'-',I2.2,' ',I2.2,':',I2.2,':',I2.2,'.',I3.3)"

        write(string,fmt) date(1), date(2), date(3), date(5), date(6), date(7), date(8)
    end function
end module
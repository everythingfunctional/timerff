module day_of_year_test
    use iso_varying_string, only: operator(//)
    use strff, only: to_string
    use timerff_impl, only: day_of_year, count_kind
    use veggies, only: &
            example_t, &
            input_t, &
            result_t, &
            test_item_t, &
            assert_equals, &
            describe, &
            fail, &
            it

    implicit none
    private
    public :: test_day_of_year

    type, extends(input_t) :: day_input
        integer(count_kind) :: year, month, day, expected
    end type

    integer, parameter :: default_integer = kind(default_integer)
contains
    function test_day_of_year() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
                "day of year", &
                [ it( &
                        "matches a set of examples", &
                        [ example_t(day_input(2024, 1, 1, 1)) &
                        , example_t(day_input(2024, 1, 2, 2)) &
                        , example_t(day_input(2024, 1, 31, 31)) &
                        , example_t(day_input(2024, 2, 1, 32)) &
                        , example_t(day_input(2024, 2, 29, 60)) &
                        , example_t(day_input(2024, 3, 1, 61)) &
                        , example_t(day_input(2024, 12, 31, 366)) &
                        , example_t(day_input(2025, 1, 1, 1)) &
                        , example_t(day_input(2025, 1, 2, 2)) &
                        , example_t(day_input(2025, 1, 31, 31)) &
                        , example_t(day_input(2025, 2, 1, 32)) &
                        , example_t(day_input(2025, 2, 28, 59)) &
                        , example_t(day_input(2025, 3, 1, 60)) &
                        , example_t(day_input(2025, 12, 31, 365)) &
                        ], &
                        check_day_of_year) &
                ])
    end function

    function check_day_of_year(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (day_input)
            result_ = assert_equals( &
                    int(input%expected, kind=default_integer), &
                    int(day_of_year(input%year, input%month, input%day), kind=default_integer), &
                    "year: " // to_string(input%year) // " month: " // to_string(input%month) // " day: " // to_string(input%day))
        class default
            result_ = fail("Didn't get a day input")
        end select
    end function
end module